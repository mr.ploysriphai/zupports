// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/color-mode',
    '@nuxt/image',
    'nuxt-icon',
    'nuxt-lodash',
  ],
  css: [
    { src: '~/assets/css/font.css' },
    { src: '~/assets/css/app.css'  },
  ],
  runtimeConfig: {
    // Config within public will be also exposed to the client
    public: {
      apiBase: process.env.API_URL
    }
  },
})
