export default defineEventHandler(async (event) => {
  const config = useRuntimeConfig()
  const body = await readBody(event)
  const items = await $fetch(`${config.public.apiBase}/restaurants/place/autocomplete`, {
    method: 'POST',
    body: { keyword: body.keyword },
  })
  .catch((error) => error.data)

  return items
  // }
})
