export default defineEventHandler(async (event) => {
  const config = useRuntimeConfig()
  const body = await readBody(event)
  const items = await $fetch(`${config.public.apiBase}/restaurants`, {
    method: 'POST',
    body: {
      keyword: body.keyword,
      pageToken: body.pageToken,
    },
  })
  .catch((error) => error.data)

  return items
})
