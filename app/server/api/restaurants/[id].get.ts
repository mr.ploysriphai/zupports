export default defineEventHandler(async (event) => {
  const config = useRuntimeConfig()
  const id = getRouterParam(event, 'id')

  const item = await $fetch(`${config.public.apiBase}/restaurants/${id}`)
  .catch((error) => error.data)
  return item
})
