# zupports

## Getting started

##
```bash
git clone https://gitlab.com/mr.ploysriphai/zupports.git
```
## Laradock
```bash
git submodule update --init
```

## API
```bash
cp ./api/.env.example ./api/.env
```

## Redis Config
```bash
echo "REDIS_HOST=redis\n\
REDIS_PASSWORD=secret_redis\n\
REDIS_PORT=6379\n\
REDIS_CLIENT=predis" >> ./api/.env
```

### Google Map api
```bash
echo GOOGLE_MAP_API= >> ./api/.env
```

## APP
```bash
echo "API_URL=http://127.0.0.1:81" > ./app/.env
```
## NGINX
```bash
cp ./nginx/zupports.conf ./laradock/nginx/sites/zupports.conf
```

### Staart Container
```bash
cd laradock && cp .env.example && docker-compose up -d nginx redis
```

### Staart Service
```bash
docker exec -it laradock-workspace-1 /bin/sh -c "cd /var/www/api && composer install"
```

### Start service app with Dev mode
```bash
docker exec -it laradock-workspace-1 /bin/sh -c "cd /var/www/app; npm install; npm run dev"
```
### Start service app with Production mode
```bash
docker exec -it laradock-workspace-1 /bin/sh -c "cd /var/www/app; npm install; npm run build; npm run start"
```
