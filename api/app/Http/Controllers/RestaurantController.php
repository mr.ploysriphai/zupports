<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use App\Restaurant;

class RestaurantController extends Controller
{
  public $cacheSeconds;

  function __construct() {
    $this->restaurant = new Restaurant;
    $this->cacheSeconds = 3600;
  }

  public function getItems(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'keyword' => 'required',
    ]);

    if ($validator->fails()):
      return response()->json([
        'statusCode' => 400,
        'errors' => $validator->errors()->messages(),
      ]);
    endif;

    $keyword = $request->get('keyword');
    $pageToken = $request->has('pageToken') ? $request->get('pageToken') : '';
    $cacheKey = "[restaurant][$keyword][$pageToken]";

    if (app('redis')->exists($cacheKey)):
      $response = json_decode(app('redis')->get($cacheKey));
      return response()->json($response);
    endif;

    $geocode = $this->restaurant->getGeocode(['address' => $keyword]);

    $findKeyword = $geocode['results'][0]['address_components'][0]['long_name'];
    $lat = $geocode['results'][0]['geometry']['location']['lat'];
    $lng = $geocode['results'][0]['geometry']['location']['lng'];
    $location = join(',', [$lat, $lng]);

    $params = [
      'location' => $location,
      'pageToken' => $pageToken,
    ];
    $locationNearBy = $this->restaurant->getPlaceNearBySearch($params);

    $items = [];
    foreach ($locationNearBy['results'] as $item):
      array_push($items, [
        'business_status' => $item['business_status'],
        'name'            => $item['name'],
        'location'        => $item['geometry']['location'],
        'photo' => [
          'reference'     => isset($item['photos']) ? $item['photos'][0]['photo_reference'] : '',
          'height'        => isset($item['photos']) ? $item['photos'][0]['height'] : '',
          'width'         => isset($item['photos']) ? $item['photos'][0]['width'] : '',
        ],
        'place_id'        => $item['place_id'],
        'rating'          => isset($item['rating']) ? $item['rating'] : '',
        'reference'       => $item['reference'],
      ]);
    endforeach;

    $response = [
      'statusCode' => 200,
      'current_page_token' => $pageToken,
      'next_page_token' => isset($locationNearBy['next_page_token'])
        ? $locationNearBy['next_page_token']
        : null,
      'results' => $items,
    ];

    app('redis')->set($cacheKey, json_encode($response));
    app('redis')->expire($cacheKey, $this->cacheSeconds);

    return response()->json($response);
  }

  public function getDetail(string $id, Request $request)
  {
    $cacheKey = "[restaurant][detail][$id]";
    if (app('redis')->exists($cacheKey)):
      $response = json_decode(app('redis')->get($cacheKey));
      return response()->json($response);
    endif;

    $item = $this->restaurant->getDetail($id);
    $item = $item['result'];

    $response = [
      'statusCode' => 200,
      'results' => [
        'business_status'       => $item['business_status'],
        'name'                  => $item['name'],
        'location'              => $item['geometry']['location'],
        'photos'                => $item['photos'],
        'place_id'              => $item['place_id'],
        'rating'                => isset($item['rating']) ? $item['rating'] : '',
        'reference'             => $item['reference'],
        'formatted_address'     => $item['formatted_address'],
        'current_opening_hours' => $item['current_opening_hours'],
        'formatted_phone_number' => $item['formatted_phone_number'],
        'reviews'               => isset($item['reviews']) ? $item['reviews'] : [],
      ]
    ];

    app('redis')->set($cacheKey, json_encode($response));
    app('redis')->expire($cacheKey, $this->cacheSeconds);

    return response()->json($response);
  }

  public function postPlaceAutocomplete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'keyword' => 'required',
    ]);

    if ($validator->fails()):
      return response()->json([
        'statusCode' => 400,
        'errors' => $validator->errors()->messages(),
      ]);
    endif;

    $items = $this->restaurant->placeAutocomplete([
      'input' => $request->get('keyword'),
    ]);

    $response = [];
    foreach ($items['predictions'] as $item):
      array_push($response, [
        'description' => $item['description'],
        'place_id' => $item['place_id'],
        'reference' => $item['reference'],
        'types' => $item['types'],
      ]);
    endforeach;

    return response()->json([
      'statusCode' => 200,
      'results' => $response,
    ]);
  }

  function getCoverPhoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'reference' => 'required',
    ]);

    if ($validator->fails()):
      return response()->json([
        'statusCode' => 400,
        'errors' => $validator->errors()->messages(),
      ]);
    endif;

    $reference = $request->get('reference');
    $width = $request->get('width') ? $request->get('width') :  120;
    $response = $this->restaurant->getPhoto([
      'width' => $width,
      'reference' => $reference,
    ]);

    return response($response->body(), $response->status())
      ->header('Content-Type', 'image/jpeg')
      ->header('Cache-Control', 'max-age=604800');
  }
}
