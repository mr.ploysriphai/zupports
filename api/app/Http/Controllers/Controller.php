<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
  public $cacheSeconds;

  function __construct() {
    $this->cacheSeconds = 180;
  }
}
