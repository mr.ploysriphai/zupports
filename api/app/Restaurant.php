<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Filesystem\Filesystem;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Http;

class Restaurant extends Model
{
  function __construct() {
    $this->radius = 50000;
  }

  function getGeocode(Array $params)
  {
    $client = new Client();
    $response = $client->request('GET', "https://maps.googleapis.com/maps/api/geocode/json", [
      'query' => [
        'address' => $params['address'],
        'key' => env('GOOGLE_MAP_API'),
      ],
    ]);

    return json_decode($response->getBody(), true);
  }

  function getPlaceNearBySearch(Array $params)
  {
    $client = new Client();
    $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/nearbysearch/json', [
      'query' => [
        'type' => 'restaurant',
        'pagetoken' => $params['pageToken'],
        'radius' => $this->radius,
        'location' => $params['location'],
        'key' => env('GOOGLE_MAP_API'),
      ]
    ]);

    return json_decode($response->getBody(), true);
  }

  function getDetail($id)
  {
    $fields = [
      'business_status',
      'name',
      'geometry',
      'photos',
      'place_id',
      'rating',
      'reference',

      'formatted_address',
      'current_opening_hours',
      'formatted_phone_number',

      'reviews',
    ];

    $client = new Client();
    $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/details/json', [
      'query' => [
        'fields' => join(',', $fields),
        'language' => 'th-TH',
        'reviews_no_translations' => true,
        'place_id' => $id,
        'key' => env('GOOGLE_MAP_API'),
      ]
    ]);

    return json_decode($response->getBody(), true);
  }

  function getPhoto(Array $params)
  {
    return Http::get('https://maps.googleapis.com/maps/api/place/photo', [
      'maxwidth' => $params['width'],
      'photo_reference' => $params['reference'],
      'key' => env('GOOGLE_MAP_API'),
    ]);
  }

  function placeAutocomplete(Array $params)
  {
    $client = new Client();
    $response = $client->request('GET', "https://maps.googleapis.com/maps/api/place/autocomplete/json", [
      'query' => [
        'input' => $params['input'],
        'radius' => $this->radius,
        'components' => 'country:th',
        'language' => 'th',
        'key' => env('GOOGLE_MAP_API'),
      ]
    ]);

    return json_decode($response->getBody(), true);
  }
}
