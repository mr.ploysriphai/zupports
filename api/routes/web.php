<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// use App\Http\Controllers\RestaurantController;
$router->get('/', function () use ($router) {
  return $router->app->version();
});

$router->post('restaurants', 'RestaurantController@getItems');
$router->get('restaurants/{id}', 'RestaurantController@getDetail');
$router->get('restaurants/photo/cover', 'RestaurantController@getCoverPhoto');
$router->post('restaurants/place/autocomplete', 'RestaurantController@postPlaceAutocomplete');
